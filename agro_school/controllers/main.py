                    
import logging
import werkzeug.wrappers
from datetime import date
import pytz
from odoo import http, SUPERUSER_ID
from odoo.http import request
import json

try:
    import simplejson as json
except ImportError:
    import json

_logger = logging.getLogger(__name__)

DATETIMEFORMAT = '%Y-%m-%d %H:%M:%S'
DATEFORMAT = '%Y-%m-%d'
LOCALTZ = pytz.timezone('Asia/Jakarta')

class DateEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, date):
            return str(obj)
        return json.JSONEncoder.default(self, obj)


def valid_response(status, data):
    return werkzeug.wrappers.Response(
        status=status,
        content_type='application/json; charset=utf-8',
        response=json.dumps(data, cls=DateEncoder),
    )

def default_response(response):
    return {
        "jsonrpc": "2.0",
        "id": False,
        "result": response
    }



APIKEY = '$2y$10$t7ZPJgiZBJWvH36ceZPU1ONij9F2Who0xl.632mRpQbMhTFAUHlNC'

class AgroInterface(http.Controller):
    @http.route('/api/v1/get_all_teacher', type='json', auth="public", csrf=False)
    def getAllTeacher(self,values=None):
        if request.httprequest.headers.get('Api-key'):
            key = request.httprequest.headers.get('Api-key')

            if key == APIKEY:
                # Check in partner data email or phone
                # Check for phone or email has been exist
                if request.httprequest.data:
                    values = json.loads(request.httprequest.data)
                else:
                    result = {"code": 4,
                              "desc": 'Data is Empty'}

                    return result
                result = {}
                vals = []
                teachers = request.env['agro.teacher'].sudo().search([])
                if teachers:
                    for teacher in teachers:
                        vals.append({
                            'name' : teacher.name,
                            'mobile_phone' : teacher.mobile_phone,
                        })
                    result = {
                        "code": 2,
                        "data": vals,
                        "desc": "Success" 
                    }
                    return result
                else:
                    result = {
                            "code": 3,
                            "desc": "Teacher not found" 
                        }

        else:
            result = {
                "code": 3,
                "desc": 'Failed to authentication'}
            return result
        
    @http.route('/api/v1/get_teacher_by_phone', type='json', auth="public", csrf=False)
    def get_techer_by_phone(self,values=None):
        if request.httprequest.headers.get('Api-key'):
            key = request.httprequest.headers.get('Api-key')

            if key == APIKEY:
                # Check in partner data email or phone
                # Check for phone or email has been exist
                if request.httprequest.data:
                    values = json.loads(request.httprequest.data)
                else:
                    result = {"code": 4,
                              "desc": 'Data is Empty'}

                    return result
                datas = values
                result = {}
                print(datas, 'DKOWAKODWKODWAOKDKAOW')
                if datas:
                    mobile_phone = datas['mobile_phone']
                    if not mobile_phone:
                        result = {
                            "code": 3,
                            "data": [],
                            "desc": 'Param mobile_phone is Required'
                        }
                        return result
                    teacher_id = request.env['agro.teacher'].sudo().search([('mobile_phone', '=', mobile_phone)], limit=1)
                    print('dkwakowaowa', teacher_id)
                    if teacher_id:
                        result = {
                            "code": 2,
                            "data": {
                                'name' : teacher_id.name,
                                'mobile_phone' : teacher_id.mobile_phone,
                            },
                            "desc": "Success" 
                        }
                        return result
                    else:
                        result = {
                                "code": 3,
                                "desc": "Teacher not found" 
                            }
                        return result

        else:
            result = {
                "code": 3,
                "desc": 'Failed to authentication'}
            return result

    @http.route('/api/v1/get_state', type='json', auth="public", csrf=False)
    def getState(self,values=None):
        if request.httprequest.headers.get('Api-key'):
            key = request.httprequest.headers.get('Api-key')

            if key == APIKEY:
                # Check in partner data email or phone
                # Check for phone or email has been exist
                if request.httprequest.data:
                    values = json.loads(request.httprequest.data)
                else:
                    result = {"code": 4,
                              "desc": 'Data is Empty'}

                    return result
                result = {}
                vals = []
                state_ids = request.env['res.country.state'].sudo().search([])
                if state_ids:
                    for state in state_ids:
                        vals.append({
                            'id' : state.id,
                            'name' : state.name,
                        })
                    result = {
                        "code": 2,
                        "data": vals,
                        "desc": "Success" 
                    }
                    return result
                else:
                    result = {
                            "code": 3,
                            "desc": "State not found" 
                        }

        else:
            result = {
                "code": 3,
                "desc": 'Failed to authentication'}
            return result
        
    @http.route('/api/v1/get_country', type='json', auth="public", csrf=False)
    def getCountry(self,values=None):
        if request.httprequest.headers.get('Api-key'):
            key = request.httprequest.headers.get('Api-key')

            if key == APIKEY:
                # Check in partner data email or phone
                # Check for phone or email has been exist
                if request.httprequest.data:
                    values = json.loads(request.httprequest.data)
                else:
                    result = {"code": 4,
                              "desc": 'Data is Empty'}

                    return result
                result = {}
                vals = []
                country_ids = request.env['res.country'].sudo().search([])
                if country_ids:
                    for country in country_ids:
                        vals.append({
                            'id' : country.id,
                            'name' : country.name,
                        })
                    result = {
                        "code": 2,
                        "data": vals,
                        "desc": "Success" 
                    }
                    return result
                else:
                    result = {
                            "code": 3,
                            "desc": "Country not found" 
                        }

        else:
            result = {
                "code": 3,
                "desc": 'Failed to authentication'}
            return result
        
    @http.route('/api/v1/get_major', type='json', auth="public", csrf=False)
    def getMajor(self,values=None):
        if request.httprequest.headers.get('Api-key'):
            key = request.httprequest.headers.get('Api-key')

            if key == APIKEY:
                # Check in partner data email or phone
                # Check for phone or email has been exist
                if request.httprequest.data:
                    values = json.loads(request.httprequest.data)
                else:
                    result = {"code": 4,
                              "desc": 'Data is Empty'}

                    return result
                result = {}
                vals = []
                majors = request.env['agro.major'].sudo().search([])
                if majors:
                    for major in majors:
                        vals.append({
                            'id' : major.id,
                            'name' : major.name,
                        })
                    result = {
                        "code": 2,
                        "data": vals,
                        "desc": "Success" 
                    }
                    return result
                else:
                    result = {
                            "code": 3,
                            "desc": "Country not found" 
                        }

        else:
            result = {
                "code": 3,
                "desc": 'Failed to authentication'}
            return result
        
    @http.route('/api/v1/create_student', type='json', auth="public", csrf=False)
    def create_student(self,values=None):
        if request.httprequest.headers.get('Api-key'):
            key = request.httprequest.headers.get('Api-key')

            if key == APIKEY:
                # Check in partner data email or phone
                # Check for phone or email has been exist
                if request.httprequest.data:
                    values = json.loads(request.httprequest.data)
                else:
                    result = {"code": 4,
                              "desc": 'Data is Empty'}

                    return result
                data = values
                vals = []
                result = []
                if data:
                    check_major_id = request.env['agro.major'].sudo().search([('id', '=', data['major_id'])])
                    if not check_major_id:
                        result = {
                            "code": 3,
                            "desc": "Wrong ID on major_id" 
                        }
                        return result
                    
                    check_state_id = request.env['res.country.state'].sudo().search([('id', '=', data['state_id'])])
                    if not check_state_id:
                        result = {
                            "code": 3,
                            "desc": "Wrong ID on state_id" 
                        }
                        return result
                    
                    check_country_id = request.env['res.country'].sudo().search([('id', '=', data['country_id'])])
                    if not check_country_id:
                        result = {
                            "code": 3,
                            "desc": "Wrong ID on country_id" 
                        }
                        return result
                    vals.append({
                        'name' : data['name'],
                        'nis' : data['nis'],
                        'nisn' : data['nisn'],
                        'major_id': data['major_id'],
                        'street' : data['street'],
                        'street2' : data['street2'],
                        'city' : data['city'],
                        'zip' : data['zip'],
                        'state_id' : data['state_id'],
                        'room_class' : data['class'],
                        'mobile_phone' : data['mobile_phone'],
                        'country_id' : data['country_id'],
                    })
                    student = request.env['agro.student'].sudo().create(vals)
                    if student:
                        result = {
                            "code": 2,
                            "data": vals,
                            "desc": "Success" 
                        }
                    else:
                        result = {
                            "code": 2,
                            "data": vals,
                            "desc": "Failed Create Student" 
                        }
                    return result   
                
            else:
                result = {
                    "code": 3,
                    "desc": 'Failed to authentication'}
                return result