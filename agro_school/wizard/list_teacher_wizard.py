from odoo import models, fields, api, _
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError

class WzJamMengajarGuru(models.TransientModel):
    _name = 'list.teacher.wizard'
    _description = 'List Teacher Wizard'

    teacher_ids = fields.Many2many('agro.teacher', string='Teachers')

    def generate(self):
        active_ids = self.env.context.get('active_ids', [])
        datas = {
            'ids': active_ids,
            'model': 'agro.teacher',
            'form': self.read()[0]
        }
        return self.env.ref('agro_school.action_list_teacher_report').report_action([], data=datas)