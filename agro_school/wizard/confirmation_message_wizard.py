from odoo import api, fields, models


class ConfirmationMessageWizard(models.TransientModel):
    _name = 'confirmation.message.wizard'
    _description = 'Confirmation Message Wizard'

    def button_action(self):
        """ function to trigger action """
        active_id = self._context.get('student_id', False)
        student = self.env['agro.student'].browse(active_id)
        action_type = self._context.get('action_type', '')
        print(student, 'DKWOAOKWAKOWAOKODKWA')
        result = False

        if student and action_type:
            if action_type == 'active':
                result = student.active = True

            if action_type == 'non_active':
                result = student.active =  False
        return result
