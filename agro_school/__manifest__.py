# -*- coding: utf-8 -*-
{
    'name': "School Module",

    'summary': """
        """,

    'description': """
        Custome Module For Manage (Teachers, Students, Class) and also integration to get teacher and create student
    """,

    'author': "Gilang Daruri",
    'website': "",

    'category': 'education',
    'version': '',
    'license': 'LGPL-3',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale_management', 'account',],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'reports/list_teacher_report.xml',
        'reports/kwitansi_siswa_report.xml',
        'reports/report_data.xml',
        'data/ir_cron.xml',
        
        'views/menu.xml',
        'wizard/confirmation_message_wizard_views.xml',
        'views/teacher_views.xml',
        'views/major_views.xml',
        'views/student_views.xml',
        'views/study_group_views.xml',
        'views/account_move.xml',
        'views/class_activity_views.xml',
        'views/product_views.xml',
        'wizard/list_teacher_wizard.xml',
    ],
}
