from . import teacher
from . import student
from . import major
from . import study_group
from . import class_activity
from . import product
from . import account_move