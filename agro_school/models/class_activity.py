from odoo import _, models, api, fields

class ClassActivity(models.Model):
    _name = 'class.activity'
    _description = 'Class Activity'
    _order = 'date Desc'
    
    teacher_id = fields.Many2one('agro.teacher', string='Teacher')
    study_group_id = fields.Many2one('study.group', string='Study Group')
    date = fields.Datetime(default=fields.Datetime.now())
    major_id = fields.Many2one('agro.major', string='Major', related="study_group_id.major_id", store=True)
    room_class = fields.Selection(related="study_group_id.room_class", store=True)
    line_ids = fields.One2many('class.activity.line', 'activity_id', string='Lines')
    
    @api.onchange('study_group_id')
    def _onchange_study_group_id(self):
        students = []
        self.line_ids = False
        if self.study_group_id:
            for stud in self.study_group_id.line_ids:
                students.append((0,0, {
                    'student_id' : stud.id,
                }))
            self.line_ids = students
            
            
class ClassActivityLine(models.Model):
    _name = 'class.activity.line'
    _description = 'Class Activity Lines'
    
    activity_id = fields.Many2one('class.activity', string='Activity')
    student_id = fields.Many2one('agro.student', string='Student')
    nis = fields.Char(related="student_id.nis", store=True)
    nisn = fields.Char(related="student_id.nisn", store=True)