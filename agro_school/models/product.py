from odoo import _, fields, models, api

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    is_school_product = fields.Boolean()

class ProductProduct(models.Model):
    _inherit = 'product.product'

    is_school_product = fields.Boolean(related="product_tmpl_id.is_school_product", store=True, readonly=False)