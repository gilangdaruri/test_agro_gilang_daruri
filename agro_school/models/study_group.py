from odoo import _, models, api, fields

class StudyGroup(models.Model):
    _name = 'study.group'
    _description = 'Study Group'
    _rec_name = 'name'
    
    name = fields.Char()
    major_id = fields.Many2one('agro.major', string='Major')
    room_class = fields.Selection([
        ('10', 'X'),
        ('11', 'XI'),
        ('12', 'XII'),
    ], string="Class")
    line_ids = fields.One2many('study.group.line', 'study_group_id', string='Students')
    
class StudyGroupLine(models.Model):
    _name = 'study.group.line'
    _description = 'Study Group Lines'
    
    study_group_id = fields.Many2one('study.group', string='Study Group')
    student_id = fields.Many2one('agro.student', string='Student')
    nis = fields.Char(related="student_id.nis", store=True)
    nisn = fields.Char(related="student_id.nisn", store=True)