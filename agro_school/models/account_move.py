from odoo import _, models, api, fields

class AccountMoveInherit(models.Model):
    _inherit = 'account.move'
    
    def print_kwitansi(self):
        active_ids = self.env.context.get('active_ids', [])
        datas = {
            'ids': active_ids,
            'model': 'account.move',
            'form': self.read()[0]
        }
        return self.env.ref('agro_school.action_kwitansi_siswa_report').report_action([], data=datas)