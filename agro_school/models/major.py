from odoo import _, fields, models, api

class AgroMajor(models.Model):
    _name = 'agro.major'
    _description = 'majors'
    _rec_name = 'name'
    
    name = fields.Char(required=True)
    code = fields.Char(required=True)