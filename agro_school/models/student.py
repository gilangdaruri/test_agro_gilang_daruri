from odoo import _, fields, models, api
from datetime import datetime

class AgroStudent(models.Model):
    _name = 'agro.student'
    _description = 'students'
    _rec_name = 'name'
    
    name = fields.Char(required=True)
    nis = fields.Char('NIS')
    nisn = fields.Char('NISN')
    room_class = fields.Selection([
        ('10', 'X'),
        ('11', 'XI'),
        ('12', 'XII'),
    ], string="Class")
    major_id = fields.Many2one('agro.major', string='Major')
    street = fields.Char('Address Line')
    street2 = fields.Char('Address Line 2')
    postal = fields.Char('Postal Code')
    city = fields.Char('City')
    state_id = fields.Many2one('res.country.state', 'State',
                               ondelete='set null')
    country_id = fields.Many2one('res.country', 'Country',
                                 ondelete='set null')
    zip = fields.Char('Zip')
    mobile_phone = fields.Char('Phone')
    active = fields.Boolean(default=True)
    partner_id = fields.Many2one('res.partner', string='Related Partner')
    list_invoice_ids = fields.One2many('list.invoice.student', 'student_id', string='Student Invoice list')
    
    _sql_constraints = [
        ("mobile_phone_unique", "unique(mobile_phone)", _("Mobile Phone must be unique."))
    ]
    _sql_constraints = [
        ("nis_unique", "unique(nis)", _("NIS must be unique."))
    ]
    _sql_constraints = [
        ("nisn_unique", "unique(nisn)", _("NISN must be unique."))
    ]
    
    def prepare_invoice(self, student, scheduled_pay):
        vals = []
        data = {}
        if student.list_invoice_ids:
            for list in student.list_invoice_ids.filtered(lambda x:x.scheduled_pay == scheduled_pay):
                vals.append((0,0,{
                    'product_id': list.product_id.id,
                    'quantity': list.qty,
                    'price_unit': list.price,
                }))
            data = {
                'partner_id': student.partner_id.id,
                'invoice_date' : datetime.now().date(),
                'move_type' : 'out_invoice',
                'invoice_line_ids' : vals
            }
        return data
    
    def cron_invoice_student_every_month(self):
        students= self.search([])
        for student in students:
            data = self.prepare_invoice(student, 'month')
            if data:
                inv_id = self.env['account.move'].sudo().create(data)
        
    @api.model
    def create(self, vals):
        res = super(AgroStudent,self).create(vals)
        partner_id = self.env['res.partner'].sudo().create({
            'name' : res.name
        })
        res.partner_id = partner_id.id
        return res
    
class ListInvoiceStudent(models.Model):
    _name = 'list.invoice.student'
    _description = 'List Invoice Student'
    
    student_id = fields.Many2one('agro.student', string='Student')
    product_id = fields.Many2one('product.product', string='Product')
    description = fields.Text()
    scheduled_pay = fields.Selection([
        ('month', 'Month'),
        ('year', 'Year')
    ])
    qty = fields.Float()
    price = fields.Float()
    total_amount = fields.Float(compute="get_total_amount", store=True)
    amount_paid = fields.Float()
    
    @api.onchange('product_id')
    def _onchange_product_id(self):
        self.description = self.product_id.name
    
    @api.depends('qty', 'price')
    def get_total_amount(self):
        for rec in self:
            rec.total_amount = rec.qty * rec.price