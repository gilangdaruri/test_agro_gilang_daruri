from odoo import _, fields, models, api

class AgroTeacher(models.Model):
    _name = 'agro.teacher'
    _description = 'Teachers'
    _rec_name = 'name'
    
    name = fields.Char(required=True)
    street = fields.Char('Address Line 1')
    street2 = fields.Char('Address Line 2')
    postal = fields.Char('Postal Code')
    city = fields.Char('City')
    state_id = fields.Many2one('res.country.state', 'State',
                               ondelete='set null')
    country_id = fields.Many2one('res.country', 'Country',
                                 ondelete='set null')
    zip = fields.Char('Zip')
    mobile_phone = fields.Char('Phone')
    active = fields.Boolean(default=True)
    student_count = fields.Integer(compute="get_student_count", store=True)
    study_group_ids = fields.Many2many('study.group', string='Teaching In Class')
    student_ids = fields.One2many('student.line', 'teacher_id', string='Student')
    
    @api.depends('student_ids')
    def get_student_count(self):
        for rec in self:
            rec.student_count = len(rec.student_ids) if rec.student_ids else 0
            
    
    @api.onchange('study_group_ids')
    def _onchange_study_group_ids(self):
        vals = []
        self.student_ids = False
        if self.study_group_ids:
            for stud_group in self.study_group_ids:
                for stud in stud_group.line_ids:
                    vals.append((0,0, {
                        'student_id' : stud.student_id.id,
                        'study_group_id' : stud_group.id,
                    }))
        self.student_ids = vals
    
    _sql_constraints = [
        ("mobile_phone_unique", "unique(mobile_phone)", _("Mobile Phone must be unique."))
    ]
class StudentsLine(models.Model):
    _name = 'student.line'
    _description = 'Student Lines'
    
    teacher_id = fields.Many2one('agro.teacher', string='Teacher')
    student_id = fields.Many2one('agro.student', string='Student')
    nis = fields.Char(related="student_id.nis", store=True)
    nisn = fields.Char(related="student_id.nisn", store=True)
    student_status = fields.Boolean(related="student_id.active")
    study_group_id = fields.Many2one('study.group', string='Study Group')