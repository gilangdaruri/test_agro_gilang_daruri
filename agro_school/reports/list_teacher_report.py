from odoo import _, api, fields, models
from odoo.exceptions import UserError
from datetime import datetime

class ListTeacherReport(models.AbstractModel):
    _name = 'report.agro_school.list_teacher_report'
    _description = "LIST TEACHER REPORT"

    @api.model
    def _get_report_values(self, docids, data=None):
        result = []
        no = 1
        teachers = self.env['agro.teacher'].search([('id', 'in', data['form']['teacher_ids'])])
        if teachers:
            for teacher in teachers:
                result.append((0,0, {
                    'no' : no,
                    'teacher' : teacher.name,
                    'students' : teacher.student_ids,
                }))
                no += 1
        return {
            'result': result,
            'company_name': self.env.user.company_id.name,
            'company_id': self.env.user.company_id,
        }