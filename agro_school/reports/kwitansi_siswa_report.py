from odoo import _, api, fields, models
from datetime import datetime
from num2words import num2words
import string

class KwitansiSiswaReport(models.AbstractModel):
    _name = 'report.agro_school.kwitansi_siswa_report'
    _description = "KWITANSI SISWA REPORT"
    
    def terbilang(self, amount, currency):
        currency_name = 'Rupiah' if currency.name == 'IDR' else currency.name
        return string.capwords(str(num2words(amount, lang='id'))) + ' ' + currency_name

    @api.model
    def _get_report_values(self, docids, data=None):
        result = []
        student_id = False
        no = 1
        study_group_line = False
        active_id = data['context']['active_id']
        move_id = self.env['account.move'].browse(active_id)
        if move_id:
            student_id = self.env['agro.student'].search([('partner_id', '=', move_id.partner_id.id)], limit=1)
            if student_id:
                study_group_line = self.env['study.group.line'].search([('student_id', '=', student_id.id)])
        return {
            'move_id': move_id,
            'date': fields.Date.today().strftime('%d %b %Y'),
            'hour': fields.Datetime.now().strftime('%H:%M:%S'),
            'nis' : student_id.nis if student_id else '',
            'self': self,
            'class' : study_group_line.study_group_id.name if study_group_line else ''
        }